# README #

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Download](https://api.bintray.com/packages/caramba/maven/de.elomagic.parent-pom/images/download.svg)](https://bintray.com/caramba/maven/de.elomagic.parent-pom/_latestVersion)

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Most elomagic projects based on this parent POM

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Maven Tips

* Build project:

    ```mvn clean install```
    
* Release project

    ```mvn clean install release:prepare release:perform```
    