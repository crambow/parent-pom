#!/bin/bash



sed -i~ "/<servers>/ a\
<server>\
  <id>elo-repository</id>\
  <username>$REP_UID</username>\
  <password>$REP_PWD</password>\
</server>\
<server>\
  <id>bintray</id>\
  <username>$REP_BINTRAY_UID</username>\
  <password>$REP_BINTRAY_PWD</password>\
</server>" /usr/share/maven/conf/settings.xml


